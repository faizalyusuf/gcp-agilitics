resource "google_compute_instance_template" "web_server" {
  name = "${var.app_name}-web-server-template"
  description = "Template to create apache server"
  instance_description = "Apache server"
  #can_ip_forward = false
  machine_type = "n1-standard-1"
  tags = ["http"]  

  scheduling {
    automatic_restart = true
  }  

  disk {
    source_image = "debian-cloud/debian-10"
    auto_delete = true
    boot = true
  }
  
  network_interface {
    network = google_compute_network.vpc.name
    subnetwork = google_compute_subnetwork.private_subnet_1.name
  }
  
  lifecycle {
    create_before_destroy = true
  }  

  metadata_startup_script = "apt-get update;\napt-get install -y git\napt-get install -y apache2\ngit clone https://github.com/learngcpwithmahesh/MountkirkGames.git;\ncp -r MountkirkGames/* /var/www/html;\nsystemctl enable apache;\nsystemctl restart apache2"
}