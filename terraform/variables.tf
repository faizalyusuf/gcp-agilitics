
variable "gcp_project" {
  type = string
  default = "smiling-boy"
}

variable "app_name" {
  type = string
  default = "mountkirk-games"
}

variable "app_project" {
  type = string
  default = "smiling-boy"
}

variable "gcp_region_1" {
  type = string
  default = "asia-southeast1"
}

variable "gcp_zone_1" {
  type = string
  default = "asia-southeast1-a"
}

variable "private_subnet_cidr_1" {
  type = string
  default = "10.1.0.0/24"
}

variable "lb_max_replicas" {
  type = string
  description = "Max number of vm replicas"
  default = "4"
}
 
variable "lb_min_replicas" {
  type = string
  description = "Min number of vm replicas"
  default = "2"
}

